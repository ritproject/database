defmodule RitDatabase.MixProject do
  use Mix.Project

  def project do
    [
      app: :rit_database,
      name: "Rit Database",
      description: "Database Manager for Rit",
      source_url: "https://gitlab.com/ritproject/database",
      version: "0.0.1",
      elixir: "~> 1.9",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:gettext] ++ Mix.compilers(),
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),
      package: package(),
      preferred_cli_env: preferred_cli_env(),
      releases: releases(),
      test_coverage: [tool: ExCoveralls]
    ]
  end

  def application do
    [
      mod: {RitDatabase.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  defp package do
    [
      licenses: ["AGPL-3.0-or-later"],
      links: %{
        "GitLab" => "https://gitlab.com/ritproject/database"
      }
    ]
  end

  defp releases do
    [
      rit_database: [
        include_executables_for: [:unix],
        include_erts: false
      ]
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  defp deps do
    [
      {:argon2_elixir, "~> 2.0.5"},
      {:credo, "~> 1.1.2", only: [:dev, :test], runtime: false},
      {:dataloader, "~> 1.0.6"},
      {:ecto_sql, "~> 3.1"},
      {:ex_doc, "~> 0.21.1", only: [:dev], runtime: false},
      {:excoveralls, "~> 0.11.1", only: [:dev, :test]},
      {:gettext, "~> 0.11"},
      {:postgrex, ">= 0.0.0"}
    ]
  end

  defp aliases do
    [
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      "ecto.setup": ["ecto.bootstrap", "run priv/repo/seeds.exs"],
      "test.coverage": ["ecto.drop", "ecto.bootstrap", "coveralls"],
      "test.static": ["credo list --strict --all"]
    ]
  end

  defp preferred_cli_env do
    [
      "coveralls.detail": :test,
      "coveralls.html": :test,
      "coveralls.post": :test,
      "test.coverage": :test,
      "test.static": :test,
      coveralls: :test
    ]
  end
end
