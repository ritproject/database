defmodule RitDatabase.Accounts do
  @moduledoc """
  The Accounts context.

  Models:

  - User
  - (User) Credential
  """

  alias RitDatabase.Accounts.{CredentialManager, UserManager}

  def list_users, do: UserManager.list_users()
  def get_user!(id), do: UserManager.get_user!(id)
  def create_user(attrs), do: UserManager.create_user(attrs)
  def update_user(user, attrs), do: UserManager.update_user(user, attrs)
  def delete_user(user), do: UserManager.delete_user(user)
  def change_user(user), do: UserManager.change_user(user)

  def login!(params), do: CredentialManager.login!(params)
  def change_credential(credential), do: CredentialManager.change_credential(credential)

  def data do
    Dataloader.Ecto.new(RitDatabase.Repo, query: &query/2)
  end

  def query(queryable, _params) do
    queryable
  end
end
