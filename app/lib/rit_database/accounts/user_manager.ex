defmodule RitDatabase.Accounts.UserManager do
  @moduledoc """
  User operations:

  - List users: List all users on database
  - Get user: Retrieve user by id from database
  - Create user: Create a user and its credential
  - Update user: Update non-critical user attributes
  - Delete user: Remove user from database
  - Change user: Create a changeset from a user
  """

  import Ecto.Query, warn: false

  alias Ecto.Changeset

  alias RitDatabase.Accounts.{Credential, User}
  alias RitDatabase.Repo

  def list_users do
    User
    |> Repo.all()
    |> Repo.preload(:credential)
  end

  def get_user!(id) do
    User
    |> Repo.get!(id)
    |> Repo.preload(:credential)
  end

  def create_user(attrs \\ %{}) do
    %User{}
    |> User.create_changeset(attrs)
    |> Changeset.cast_assoc(:credential, with: &Credential.create_changeset/2)
    |> Repo.insert()
  end

  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Changeset.cast_assoc(:credential, with: &Credential.changeset/2)
    |> Repo.update()
  end

  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end
end
