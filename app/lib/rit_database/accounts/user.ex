defmodule RitDatabase.Accounts.User do
  @moduledoc """
  The user entity

  Attributes:

  - first_name
  - last_name
  - username
  - credential: Responsible for user authentication attributes
  """

  use Ecto.Schema

  import Ecto.Changeset

  alias Ecto.Changeset
  alias RitDatabase.Accounts.{Credential, User}

  schema "users" do
    field :first_name, :string
    field :last_name, :string
    field :username, :string
    has_one :credential, Credential

    timestamps()
  end

  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:first_name, :last_name])
    |> validate_required([:first_name, :last_name])
    |> validate_name(:first_name)
    |> validate_name(:last_name)
  end

  def create_changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:username, :first_name, :last_name])
    |> validate_required([:username, :first_name, :last_name])
    |> validate_name(:first_name)
    |> validate_name(:last_name)
    |> validate_username()
  end

  @name_regex ~r/^[\p{L} ]+$/u
  @name_error "should contain only letters and spaces"

  defp validate_name(%Changeset{} = changeset, field) do
    changeset
    |> validate_format(field, @name_regex, message: @name_error)
    |> update_change(field, &format_name/1)
    |> validate_length(field, min: 3, max: 50)
  end

  defp format_name(name) do
    name
    |> String.split(~r{\s+}, trim: true)
    |> Enum.map(&String.capitalize/1)
    |> Enum.join(" ")
  end

  @username_regex ~r/^[a-zA-Z][a-zA-z0-9]+$/
  @username_error "first character should be a letter and should contain only letters and numbers"

  defp validate_username(%Changeset{} = changeset) do
    changeset
    |> validate_length(:username, min: 4, max: 30)
    |> validate_format(:username, @username_regex, message: @username_error)
    |> update_change(:username, &String.downcase/1)
    |> unique_constraint(:username)
  end
end
