defmodule RitDatabase.Accounts.CredentialManager do
  @moduledoc """
  Credential operations:

  - Login: Perform login using email and password
  """

  import Ecto.Query, warn: false

  alias RitDatabase.Accounts
  alias RitDatabase.Accounts.Credential
  alias RitDatabase.Repo

  def login!(%{"email" => email, "password" => password}) do
    do_login!(email, password)
  end

  def login!(%{email: email, password: password}) do
    do_login!(email, password)
  end

  def sign_up(attrs \\ %{}) do
    Accounts.create_user(attrs)
  end

  def change_credential(%Credential{} = credential) do
    Credential.changeset(credential, %{})
  end

  defp do_login!(email, password) do
    {:ok, %Credential{user_id: user_id}} =
      Credential
      |> Repo.get_by!(email: email)
      |> Argon2.check_pass(password)

    Accounts.get_user!(user_id)
  end
end
