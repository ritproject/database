defmodule RitDatabase.Application do
  @moduledoc false

  use Application

  @impl Application
  @spec start(any(), any()) :: {:ok, pid} | {:ok, pid(), any()} | {:error, reason :: term()}
  def start(_type, _args) do
    bootstrap()

    children = [
      RitDatabase.Repo
    ]

    opts = [strategy: :one_for_one, name: RitDatabase.Supervisor]

    Supervisor.start_link(children, opts)
  end

  defp bootstrap do
    if Application.get_env(:rit_database, :environment) == :prod do
      RitDatabase.Tasks.bootstrap()
    end
  end
end
