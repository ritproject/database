defmodule RitDatabase.Repo do
  @moduledoc false

  use Ecto.Repo,
    otp_app: :rit_database,
    adapter: Ecto.Adapters.Postgres
end
