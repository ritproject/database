defmodule RitDatabase.Tasks do
  @moduledoc """
  Tasks are functions intended to be used to integrate external services or
  bootstrap components. It is used by RitDatabase.Application to bootstrap the
  database before the start of the application and can be executed using the
  eval command on release CLI:

  ```elixir
  rit_database eval "RitDatabase.Tasks.function(params)"
  ```
  """

  alias RitDatabase.Tasks.Database

  @spec bootstrap(any) :: :ok | {:error, atom()}
  def bootstrap(options \\ []) do
    config_options = Application.get_env(:rit_database, :rit_bootstrap, [])

    if Enum.empty?(options) do
      bootstrap_database(config_options)
    else
      bootstrap_database(Keyword.merge(config_options, options))
    end
  end

  defp bootstrap_database(options) do
    case Keyword.get(options, :create_database) do
      nil -> :ok
      create_options -> Database.create(create_options)
    end
  end
end
