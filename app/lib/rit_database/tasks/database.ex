defmodule RitDatabase.Tasks.Database do
  @moduledoc """
  Database tasks are functions that integrate RitDatabase with PostgreSQL and
  execute changes on it.
  """

  alias Ecto.Migrator

  require Logger

  @spec create(keyword()) :: :ok | {:error, atom()}
  def create(create_options \\ []) do
    Enum.each(repos(), fn repo -> create_repo(create_options, repo) end)
  end

  @spec create_repo(keyword(), [atom()]) :: :ok | {:error, atom()}
  def create_repo(create_options \\ [], repo) do
    case repo.__adapter__.storage_up(repo.config) do
      :ok ->
        Logger.info("The database for #{inspect(repo)} has been created.")
        maybe_migrate_repo(create_options, repo, true)

      {:error, :already_up} ->
        Logger.info("The database for #{inspect(repo)} has already been created.")
        maybe_migrate_repo(create_options, repo, false)

      {:error, term} ->
        raise "The database for #{inspect(repo)} couldn't be created: #{inspect(term)}."

      error ->
        raise "The database for #{inspect(repo)} couldn't be created: #{inspect(error)}."
    end
  rescue
    _error ->
      Logger.error("The database for #{inspect(repo)} couldn't be created.")
      handle_create_error(create_options)
  end

  @spec migrate :: :ok
  def migrate do
    Enum.each(repos(), &migrate_repo/1)
  end

  @spec migrate_repo(atom()) :: :ok
  def migrate_repo(repo) do
    case Migrator.with_repo(repo, &Migrator.run(&1, :up, all: true)) do
      {:ok, _, _} -> :ok
      error -> raise "Migrate failed: #{inspect(error)}"
    end
  rescue
    _error ->
      Logger.error("The migration for #{inspect(repo)} failed.")
      {:error, :migrate_failed}
  end

  @spec rollback_repo(any, any) :: :ok | {:error, atom()}
  def rollback_repo(repo, version) do
    case Migrator.with_repo(repo, &Migrator.run(&1, :down, to: version)) do
      {:ok, _, _} -> :ok
      error -> raise "Rollback failed: #{inspect(error)}"
    end
  rescue
    _error ->
      Logger.error("The migration rollback for #{inspect(repo)} (#{inspect(version)}) failed.")
      {:error, :rollback_failed}
  end

  defp repos do
    Application.load(:rit_database)
    Application.fetch_env!(:rit_database, :ecto_repos)
  end

  defp maybe_migrate_repo(options, repo, created?) do
    case Keyword.get(options, :migrate, :always) do
      :always ->
        migrate_repo(repo)

      :on_create ->
        if created? do
          migrate_repo(repo)
        else
          :ok
        end

      _ ->
        Logger.error("Failed to understand 'migrate' option. Options are: 'always', 'on_create'")
        {:error, :migrate_failed}
    end
  end

  defp handle_create_error(options) do
    if Keyword.get(options, :reattempt_on_error?, false) == true do
      inform_wait_and_attempt_to_create(options)
    else
      {:error, :create_failed}
    end
  end

  defp inform_wait_and_attempt_to_create(options) do
    attempts = Keyword.get(options, :attempts, 0) + 1
    options = Keyword.put(options, :attempts, attempts)
    until = Keyword.get(options, :maximum_attempts, :infinity)

    if until == :infinity do
      Logger.warn("Attempt #{attempts} failed.")
    else
      if attempts >= until do
        raise "Attempt #{until} of #{until} failed."
      else
        Logger.warn("Attempt #{attempts} of #{until} failed.")
      end
    end

    waiting_milliseconds = Keyword.get(options, :reattempt_in_milliseconds, 5_000)

    Logger.info("Reattempt in #{waiting_milliseconds} milliseconds.")

    :timer.sleep(waiting_milliseconds)

    create(options)
  rescue
    _error -> {:error, :create_failed}
  end
end
