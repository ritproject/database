defmodule Mix.Tasks.Ecto.Bootstrap do
  @moduledoc """
  Create database and migrate, with desired error tolerance
  """

  use Mix.Task

  @impl Mix.Task
  @shortdoc "Create database and migrate with desired error tolerance"
  @spec run(any) :: :ok
  def run(_args) do
    RitDatabase.Tasks.bootstrap()
  end
end
