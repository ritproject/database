import Config

#
# Database
#

username =
  System.get_env("RIT_DATABASE_USERNAME") ||
    raise "environment variable RIT_DATABASE_USERNAME is missing."

password =
  System.get_env("RIT_DATABASE_PASSWORD") ||
    raise "environment variable RIT_DATABASE_PASSWORD is missing."

database =
  System.get_env("RIT_DATABASE_NAME") ||
    raise "environment variable RIT_DATABASE_NAME is missing."

hostname =
  System.get_env("RIT_DATABASE_HOSTNAME") ||
    raise "environment variable RIT_DATABASE_HOSTNAME is missing."

pool_size =
  System.get_env("RIT_DATABASE_POOL_SIZE", "10")
  |> String.to_integer()

ssl? =
  System.get_env("RIT_DATABASE_SSL", "false")
  |> String.to_existing_atom()

config :rit_database, RitDatabase.Repo,
  ssl: ssl?,
  username: username,
  password: password,
  database: database,
  hostname: hostname,
  pool_size: pool_size

migrate =
  System.get_env("RIT_DATABASE_BOOTSTRAP_MIGRATE", "always")
  |> String.to_atom()

reattempt_on_error? =
  System.get_env("RIT_DATABASE_BOOTSTRAP_REATTEMPT_ON_ERROR", "true")
  |> String.to_existing_atom()

maximum_attempts =
  System.get_env("RIT_DATABASE_BOOTSTRAP_MAXIMUM_ATTEMPTS", "10")
  |> String.to_integer()

maximum_attempts =
  if maximum_attempts > 0 do
    maximum_attempts
  else
    :infinity
  end

reattempt_in_milliseconds =
  System.get_env("RIT_DATABASE_BOOTSTRAP_REATTEMPT_IN_MILLISECONDS", "5000")
  |> String.to_integer()

config :rit_database, :rit_bootstrap,
  create_database: [
    migrate: migrate,
    reattempt_on_error?: repeat?,
    maximum_attempts: until,
    reattempt_in_milliseconds: reattempt_in_milliseconds
  ]
