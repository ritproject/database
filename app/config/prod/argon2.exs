import Config

config :argon2_elixir,
  t_cost: 10,
  m_cost: 18
