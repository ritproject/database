import Config

database_connection_maximum_attempts =
  System.get_env("RIT_DATABASE_CONNECTION_MAXIMUM_ATTEMPTS", "10")
  |> String.to_integer()

database_connection_reattempt_waiting_milliseconds =
  System.get_env("RIT_DATABASE_CONNECTION_REATTEMPT_WAITING_MILLISECONDS", "5000")
  |> String.to_integer()

config :rit_database, :rit_bootstrap,
  create_database: [
    repeat?: true,
    until: database_connection_maximum_attempts,
    wait_milliseconds: database_connection_reattempt_waiting_milliseconds
  ]
